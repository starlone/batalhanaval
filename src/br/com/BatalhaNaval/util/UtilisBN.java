package br.com.BatalhaNaval.util;

public class UtilisBN {
	public static boolean checkIf2num(String s){		
		if(s.length() == 2 && checkNumber(s))			
			return true;
		return false;
	}
	
	public static boolean checkNumber(String s){
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static int numRandom(int tam){
		return (int) (Math.random() * tam);	
		
	}

}
