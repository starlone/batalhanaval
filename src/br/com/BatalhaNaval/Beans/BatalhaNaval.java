package br.com.BatalhaNaval.Beans;

import java.util.ArrayList;

import br.com.BatalhaNaval.util.UtilisBN;

public class BatalhaNaval {
	private ArrayList<Ship> ships;
	private int numOfShips;
	private int shipLength;
	private int columns;
	private int lines;	
	
	public BatalhaNaval(){
		this.numOfShips = 3;
		this.shipLength = 3;
		this.columns = 7;
		this.lines = 7;
	}
	
	public void setConfig(int numOfShips, int shipLength, int columns,int lines) {
		if (numOfShips > 0 && columns > 0 && lines > 0){
			this.numOfShips = numOfShips;
			this.shipLength = shipLength;
			this.columns = columns;
			this.lines = lines;
		}
	}
	
	public void startGame(){
		createShips();	
// Verificar onde esta os navios
//		for(Ship ship : this.ships){
//			for(String l : ship.getLocation()){
//				System.out.println(l);
//			}
//		}
	}	
	
	public boolean isEnd(){
		return this.ships.isEmpty();
	}
	
	public ArrayList<Ship> getShips(){
		return this.ships;
	}
	
	public void checkGuess(String guess){		
		Ship wreckedShip = null;
		for (Ship ship : this.ships) {			
			ship.checkRemoveCell(guess);			
			if(ship.getLocation().isEmpty())
				wreckedShip = ship;
		}
		if (wreckedShip != null)
			this.ships.remove(wreckedShip);		
	}
	
	private void createShips(){
		this.ships = new ArrayList<Ship>();
		for(int i = 0 ; i < this.numOfShips ; i++){
			Ship ship = new Ship(this.shipLength);
			initLocation(ship);
			ships.add(ship);
		}		
	}
	
	private boolean checkCellOccupied(String guess){
		for (Ship ship : this.ships) {
			int i = ship.getLocation().indexOf(guess);
			if (i != -1)
				return true;
		}
		return false;
	}
		
	private void initLocation(Ship ship){
		int col = 5,line = 5 ;
		
		boolean erro = true;
		boolean vert = true;
		while(erro){
			col = this.columns + 1;
			line = this.lines + 1;
			
			// Verifica se ship nao excede horizontal e vertical
			while ((line > this.lines - ship.getLength() + 1)&&
					(col > this.columns - ship.getLength() + 1))
			{
				line = UtilisBN.numRandom(this.lines) + 1;
				col = UtilisBN.numRandom(this.columns) + 1;				
			}			
			// Se não couber na horizontal
			if (col > this.columns - ship.getLength() + 1){
				erro = false;
				vert = true;
				// Verifica se não está ocupado na vertical
				for (int i = 0; i < ship.getLength(); i++) {
					String s = String.valueOf(line+i) + String.valueOf(col);
					if (checkCellOccupied(s)){
						erro = true;						
						break;
					}
				}				
			}			
			else{
				// Verifica se não está ocupado na horizontal				
				String s = String.valueOf(line) + String.valueOf(col);
				if (!checkCellOccupied(s)){
					erro = false;
					vert = false;
					for (int i = 0; i < ship.getLength(); i++) {
						s = String.valueOf(line) + String.valueOf(col+i);
						if (checkCellOccupied(s)){
							erro = true;
							break;
						}
					}		
				}
				
			}			
		}
		// Cria a lista de localização
		ArrayList<String> lista = new ArrayList<String>();
		if(vert){			
			// Vertical
			for (int i = 0; i < ship.getLength(); i++) {
				String s = (line+i+"") + (col + "");								
				lista.add(s);
			}
		}
		else{			
			// Horizontal
			for (int i = 0; i < ship.getLength(); i++) {
				String s = (line + "") + (col+i+"");								
				lista.add(s);
			}
		}
		ship.setLocation(lista);
	}

}
