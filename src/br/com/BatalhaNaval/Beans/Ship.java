package br.com.BatalhaNaval.Beans;

import java.util.ArrayList;

public class Ship {
	/**
	 * Classe que representa o navio
	 */
	private ArrayList<String> location;
	private int length;
	
	public Ship(int lenght) {
		this.length = lenght;
	}
	
	public Ship(){
		this.length = 3;
	}
	
	public void checkRemoveCell(String cell){
		int i = this.location.indexOf(cell);
		if(i != -1){
			this.location.remove(cell);
		}		
	}

	/**
	 * @return the lenght
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param lenght the lenght to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the location
	 */
	public ArrayList<String> getLocation() {
		return this.location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}
	
	
 
}
