package br.com.BatalhaNaval.executable;

import java.util.Scanner;

import br.com.BatalhaNaval.Beans.BatalhaNaval;
import br.com.BatalhaNaval.util.UtilisBN;

public class StartGameShell {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] menu = {"1 - Novo Jogo","2 - Configurar","0 - Sair"};
		BatalhaNaval game = new BatalhaNaval();
		
		int opcao = 1;
		while(opcao!=0){
			for (String string : menu) {
				System.out.println(string);				
			}			
			opcao = Integer.parseInt(getText("Opcao"));
			
			switch(opcao){
			case 1:
				
				game.startGame();				

				while(!game.isEnd()){
					String guess = "";
					do{
						System.out.print("Informe um palpite");
						guess = getText(guess);  						
					}while(!UtilisBN.checkIf2num(guess));					
					game.checkGuess(guess);
				}
				break;
			case 2:
				System.out.println(" -- Configurações --");
				System.out.println("\nInforme as seguintes informações\n\n");
				String s;
				s = getText("Numero de navios (default: 3)");
				int ns = Integer.parseInt(s);
				s = getText("Comprimento dos navios (default: 3)");
				int sl = Integer.parseInt(s);
				s = getText("Numero de linhas (default: 7)");
				int l = Integer.parseInt(s);
				s = getText("Numero de colunas (default: 7)");
				int c = Integer.parseInt(s);
				game.setConfig(ns ,sl, c, l);
				break;
			}
			
				
		} 
		System.out.println("Fim");
	}
	
	public static String getText(String text){
		System.out.print(text+": ");
		return new Scanner(System.in).next();
	}

}
